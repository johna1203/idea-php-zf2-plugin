package com.kodokux.zf2.helper;

import com.intellij.execution.ExecutionException;
import com.intellij.execution.configurations.GeneralCommandLine;
import com.intellij.execution.process.ScriptRunnerUtil;
import com.intellij.openapi.project.Project;
import com.kodokux.zf2.Settings;
import com.kodokux.zf2.Zf2Component;

/**
 * User: johna1203
 * Date: 2013/08/17
 * Time: 2:01
 */
public class PHP {
    public static String execute(String phpCode, Project project) {
        String pathToPhp = Settings.getInstance(project).phpPath;

        if (pathToPhp.isEmpty()) {
            pathToPhp = Settings.DEFAULT_PHP_PATH;
        }

        return executeWithCommandLine(pathToPhp, phpCode);
    }

//    public static String execute(String phpCode) {
//        return execute(phpCode, null);
//    }

    public static String executeWithCommandLine(String pathToPhp, String phpCode) {
        if (pathToPhp != null && !pathToPhp.isEmpty() && phpCode != null && !phpCode.isEmpty()) {
            GeneralCommandLine commandLine = new GeneralCommandLine(pathToPhp, "-r", phpCode);
            try {
                String output = ScriptRunnerUtil.getProcessOutput(commandLine);
                return output;
            } catch (ExecutionException e1) {
                e1.printStackTrace();
            }
        }
        return null;
    }

}
