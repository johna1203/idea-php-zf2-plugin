package com.kodokux.zf2;

import com.intellij.notification.Notification;
import com.intellij.notification.NotificationType;
import com.intellij.notification.Notifications;
import com.intellij.openapi.components.ProjectComponent;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VfsUtil;
import com.intellij.psi.PsiElement;
import com.kodokux.zf2.helper.PHP;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: johna1203
 * Date: 2013/08/17
 * Time: 1:56
 * To change this template use File | Settings | File Templates.
 */
public class Zf2Component implements ProjectComponent {
    private static final Logger LOG = Logger.getLogger(Zf2Component.class.getName());
    private Project project;
    private List<String> smKeys = new ArrayList<String>();

    public Zf2Component(Project project) {
        this.project = project;

//        String path = project.getBaseDir().getPath();
//        String init_auto_loader_path = path + "/init_autoloader.php";
//        String config_path = path + "/config/application.config.php";
//
//        String command = "chdir('" + path + "');require 'init_autoloader.php';" +
//                "$sm = Zend\\Mvc\\Application::init(require 'config/application.config.php')->getServiceManager();" +
//                "echo json_encode($sm->getCanonicalNames());";
//
//        LOG.info(command);
//
//        PhpRunner phpRunner = new PhpRunner();
////        phpRunner.
//
//        String execute = PHP.execute(command);
//
//        try {
//            JSONObject jsonObj = new JSONObject(execute);
//            Iterator<?> keys = jsonObj.keys();
//
//            while (keys.hasNext()) {
//                String key = (String) keys.next();
//                smKeys.add(key);
////                if (jsonObj.get(key) instanceof JSONObject) {
////                }
//            }
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//
//        LOG.info(execute);
//
//        LOG.info(project.getBaseDir().getPath());


        PHP.execute("echo 'johna'", project);


    }

    public static Zf2Component getInstance(Project project) {
        return project.getComponent(Zf2Component.class);
    }

    public void initComponent() {
        // TODO: insert component initialization logic here
    }

    public void disposeComponent() {
        // TODO: insert component disposal logic here
    }

    @NotNull
    public String getComponentName() {
        return "Zf2Component";
    }

    public void projectOpened() {
        System.out.println("projectOpened");


        String execute = PHP.execute("echo 'johna';", project);
        System.out.println(execute);


        this.checkProject();
    }

    public void projectClosed() {
        // called when project is being closed
    }

    public List<String> getSmkeys() {
        return smKeys;
    }

    public String getClass(String serviceName) {

        return "";
    }

    private void checkProject() {

        if (!this.isEnabled()) {
            if (VfsUtil.findRelativeFile(this.project.getBaseDir(), "vendor") != null
                    && VfsUtil.findRelativeFile(this.project.getBaseDir(), "config") != null
                    && VfsUtil.findRelativeFile(this.project.getBaseDir(), "module") != null
                    && VfsUtil.findRelativeFile(this.project.getBaseDir(), "data", "cache") != null
                    && VfsUtil.findRelativeFile(this.project.getBaseDir(), "vendor", "zendframework", "zendframework") != null
                    ) {
                showInfoNotification("Looks like this a Zf2 project. Enable the Zf2 Plugin in Project Settings");
            }
            return;
        }

//        if (this.getContainerFiles().size() == 0) {
//            showInfoNotification("missing at least one container file");
//        }
//
//        String urlGeneratorPath = getPath(project, Settings.getInstance(project).pathToUrlGenerator);
//        File urlGeneratorFile = new File(urlGeneratorPath);
//        if (!urlGeneratorFile.exists()) {
//            showInfoNotification("missing routing file: " + urlGeneratorPath);
//        }

    }

    public boolean isEnabled() {
        return Settings.getInstance(project).pluginEnabled;
    }

    public static boolean isEnabled(Project project) {
        return Settings.getInstance(project).pluginEnabled;
    }

    public static boolean isEnabled(@Nullable PsiElement psiElement) {
        return psiElement != null && isEnabled(psiElement.getProject());
    }

    public void showInfoNotification(String content) {
        Notification errorNotification = new Notification("Zf2 Plugin", "Zf2 Plugin", content, NotificationType.INFORMATION);
        Notifications.Bus.notify(errorNotification, this.project);
    }


}
