package com.kodokux.zf2;

import com.intellij.openapi.options.Configurable;
import com.intellij.openapi.options.ConfigurationException;
import com.intellij.openapi.project.Project;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

/**
 * Created with IntelliJ IDEA.
 * User: johna
 * Date: 2013/08/17
 * Time: 17:13
 * To change this template use File | Settings | File Templates.
 */
public class SettingsForm implements Configurable {

    private final Project project;
    private JPanel panel1;
    private JTextField phpPath;
    private JCheckBox pluginEnabled;

    public SettingsForm(@NotNull final Project project) {
        this.project = project;
    }

    @Nls
    @Override
    public String getDisplayName() {
        return "Zf2 Plugin";
    }

    @Nullable
    @Override
    public String getHelpTopic() {
        return null;
    }

    @Nullable
    @Override
    public JComponent createComponent() {
//        pluginEnabled.setSelected(getSettings().pluginEnabled);
//        phpPath.setText(getSettings().phpPath);
        return (JComponent) panel1;
    }

    @Override
    public boolean isModified() {
        return !pluginEnabled.isSelected() == getSettings().pluginEnabled
                || !phpPath.getText().equals(getSettings().phpPath)
//                        || !pathToUrlGeneratorTextField.getText().equals(getSettings().pathToUrlGenerator)
//                        || !pathToTranslationRootTextField.getText().equals(getSettings().pathToTranslation)
//                        || !symfonyContainerTypeProvider.isSelected() == getSettings().symfonyContainerTypeProvider
//                        || !objectRepositoryTypeProvider.isSelected() == getSettings().objectRepositoryTypeProvider
//                        || !objectRepositoryResultTypeProvider.isSelected() == getSettings().objectRepositoryResultTypeProvider
//                        || !objectManagerFindTypeProvider.isSelected() == getSettings().objectManagerFindTypeProvider
//
//                        || !twigAnnotateRoute.isSelected() == getSettings().twigAnnotateRoute
//                        || !twigAnnotateTemplate.isSelected() == getSettings().twigAnnotateTemplate
//                        || !twigAnnotateAsset.isSelected() == getSettings().twigAnnotateAsset
//                        || !twigAnnotateAssetTags.isSelected() == getSettings().twigAnnotateAssetTags
//
//                        || !phpAnnotateTemplate.isSelected() == getSettings().phpAnnotateTemplate
//                        || !phpAnnotateService.isSelected() == getSettings().phpAnnotateService
//                        || !phpAnnotateRoute.isSelected() == getSettings().phpAnnotateRoute
//                        || !phpAnnotateTemplateAnnotation.isSelected() == getSettings().phpAnnotateTemplateAnnotation
//
//                        || !yamlAnnotateServiceConfig.isSelected() == getSettings().yamlAnnotateServiceConfig
//
//                        || !directoryToApp.getText().equals(getSettings().directoryToApp)
//                        || !directoryToWeb.getText().equals(getSettings().directoryToWeb)
                ;
    }

    @Override
    public void apply() throws ConfigurationException {
        getSettings().pluginEnabled = pluginEnabled.isSelected();
        getSettings().phpPath = phpPath.getText();
    }

    @Override
    public void reset() {
        updateUIFromSettings();
    }


    @Override
    public void disposeUIResources() {
    }

    private void updateUIFromSettings() {
        pluginEnabled.setSelected(getSettings().pluginEnabled);
        phpPath.setText(getSettings().phpPath);
    }

    private Settings getSettings() {
        return Settings.getInstance(project);
    }
}
