package com.kodokux.zf2.util;

import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiReference;
import com.jetbrains.php.PhpIndex;
import com.jetbrains.php.lang.psi.elements.Method;
import com.jetbrains.php.lang.psi.elements.MethodReference;
import com.jetbrains.php.lang.psi.elements.PhpClass;
import com.jetbrains.php.lang.psi.elements.StringLiteralExpression;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;

public class Zf2InterfacesUtil {


    public boolean isServiceManagerGetCall(PsiElement e) {
        return isCallTo(e, new Method[]{
                getInterfaceMethod(e.getProject(), "\\Zend\\ServiceManager\\ServiceLocatorInterface", "get"),
        });
    }

    public boolean isServiceManagerGetCall(Method e) {
        return isCallTo(e, new Method[]{
                getInterfaceMethod(e.getProject(), "\\Zend\\ServiceManager\\ServiceLocatorInterface", "get"),
        });
    }

    protected boolean isCallTo(PsiElement e, Method expectedMethod) {
        return isCallTo(e, new Method[]{expectedMethod});
    }

    protected boolean isCallTo(PsiElement e, Method[] expectedMethods) {
        return isCallTo(e, expectedMethods, 1);
    }

    protected boolean isCallTo(Method e, Method[] expectedMethods) {

        PhpClass methodClass = e.getContainingClass();
        for (Method expectedMethod : Arrays.asList(expectedMethods)) {
            if (null != expectedMethod
                    && expectedMethod.getName().equals(e.getName())
                    && isInstanceOf(methodClass, expectedMethod.getContainingClass())) {
                return true;
            }
        }

        return false;
    }


    protected boolean isCallTo(PsiElement e, Method[] expectedMethods, int deepness) {
        if (!(e instanceof MethodReference)) {
            return false;
        }
        System.out.print("isCallTo0\n");
        MethodReference methodRef = (MethodReference) e;

        // resolve is also called on invalid php code like "use <xxx>"
        // so double check the method name before resolve the method
        if (!isMatchingMethodName(methodRef, expectedMethods)) {
            return false;
        }
        System.out.print("isCallTo1\n");
        PsiReference psiReference = methodRef.getReference();
        if (null == psiReference) {
            return false;
        }
        System.out.print("isCallTo2\n");
        PsiElement resolvedReference = psiReference.resolve();
        if (!(resolvedReference instanceof Method)) {
            return false;
        }

        Method method = (Method) resolvedReference;
        PhpClass methodClass = method.getContainingClass();
        System.out.print("isCallTo3\n");
        for (Method expectedMethod : Arrays.asList(expectedMethods)) {
            if (null != expectedMethod
                    && expectedMethod.getName().equals(method.getName())
                    && isInstanceOf(methodClass, expectedMethod.getContainingClass())) {
                return true;
            }
        }

        return false;
    }

    protected boolean isMatchingMethodName(MethodReference methodRef, Method[] expectedMethods) {
        for (Method expectedMethod : Arrays.asList(expectedMethods)) {
            if (expectedMethod != null) {
                System.out.println(expectedMethod.getName());
            }
            if (expectedMethod != null && expectedMethod.getName().equals(methodRef.getName())) {
                return true;
            }
        }

        return false;
    }

    @Nullable
    public static String getFirstArgumentStringValue(MethodReference e) {
        String stringValue = null;

        PsiElement[] parameters = e.getParameters();
        if (parameters.length > 0 && parameters[0] instanceof StringLiteralExpression) {
            StringLiteralExpression stringLiteralExpression = (StringLiteralExpression) parameters[0];
            stringValue = stringLiteralExpression.getText(); // quoted string
            stringValue = stringValue.substring(stringLiteralExpression.getValueRange().getStartOffset(), stringLiteralExpression.getValueRange().getEndOffset());
        }

        return stringValue;
    }

    protected Method getInterfaceMethod(Project project, String interfaceFQN, String methodName) {
        PhpIndex phpIndex = PhpIndex.getInstance(project);
        System.out.println(interfaceFQN);
        Object[] interfaces = phpIndex.getInterfacesByFQN(interfaceFQN).toArray();

        if (interfaces.length < 1) {
            System.out.println("getInterfaceMethod null");
            return null;
        }

        return findClassMethodByName((PhpClass) interfaces[0], methodName);
    }

    protected Method getClassMethod(Project project, String classFQN, String methodName) {
        PhpIndex phpIndex = PhpIndex.getInstance(project);
        Object[] classes = phpIndex.getClassesByFQN(classFQN).toArray();

        if (classes.length < 1) {
            return null;
        }

        return findClassMethodByName((PhpClass) classes[0], methodName);
    }

    protected Method findClassMethodByName(PhpClass phpClass, String methodName) {
        for (Method method : phpClass.getMethods()) {
            if (method.getName().equals(methodName)) {
                return method;
            }
        }

        return null;
    }

    protected boolean isImplementationOfInterface(PhpClass phpClass, PhpClass phpInterface) {
        if (phpClass == phpInterface) {
            return true;
        }

        for (PhpClass implementedInterface : phpClass.getImplementedInterfaces()) {
            if (isImplementationOfInterface(implementedInterface, phpInterface)) {
                return true;
            }
        }

        if (null == phpClass.getSuperClass()) {
            return false;
        }

        return isImplementationOfInterface(phpClass.getSuperClass(), phpInterface);
    }

    public boolean isInstanceOf(PhpClass subjectClass, PhpClass expectedClass) {

        System.out.print("johna");

        if (subjectClass == expectedClass) {
            return true;
        }

        if (expectedClass.isInterface()) {
            return isImplementationOfInterface(subjectClass, expectedClass);
        }

        if (null == subjectClass.getSuperClass()) {
            return false;
        }

        return isInstanceOf(subjectClass.getSuperClass(), expectedClass);
    }

    public boolean isCallTo(PsiElement e, String ClassInterfaceName, String methodName) {

        // we need a full fqn name
        if (ClassInterfaceName.contains("\\") && !ClassInterfaceName.startsWith("\\")) {
            ClassInterfaceName = "\\" + ClassInterfaceName;
        }

        return isCallTo(e, new Method[]{
                getInterfaceMethod(e.getProject(), ClassInterfaceName, methodName),
                getClassMethod(e.getProject(), ClassInterfaceName, methodName),
        });
    }

    public boolean isCallTo(Method e, String ClassInterfaceName, String methodName) {

        // we need a full fqn name
        if (ClassInterfaceName.contains("\\") && !ClassInterfaceName.startsWith("\\")) {
            ClassInterfaceName = "\\" + ClassInterfaceName;
        }

        return isCallTo(e, new Method[]{
                getInterfaceMethod(e.getProject(), ClassInterfaceName, methodName),
                getClassMethod(e.getProject(), ClassInterfaceName, methodName),
        });
    }

}
