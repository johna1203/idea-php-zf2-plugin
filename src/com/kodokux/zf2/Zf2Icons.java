package com.kodokux.zf2;

import com.intellij.openapi.util.IconLoader;

import javax.swing.*;

/**
 * @author Enrique Piatti
 */
public interface Zf2Icons {
    Icon ZF2_ICON_16x16 = IconLoader.getIcon("/icons/zf216x16.png");

}
