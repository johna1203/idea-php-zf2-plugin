package com.kodokux.zf2.servicemanager;

import com.intellij.codeInsight.lookup.LookupElement;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiPolyVariantReferenceBase;
import com.intellij.psi.ResolveResult;
import com.jetbrains.php.PhpIndex;
import com.jetbrains.php.lang.psi.elements.PhpClass;
import com.jetbrains.php.lang.psi.elements.StringLiteralExpression;
import org.jetbrains.annotations.NotNull;

import java.util.*;

/**
 * @author Adrien Brault <adrien.brault@gmail.com>
 */
public class ServiceReference extends PsiPolyVariantReferenceBase<PsiElement> {

    private String serviceId;

    public ServiceReference(@NotNull PsiElement element, String ServiceId) {
        super(element);
        serviceId = ServiceId;
    }

    public ServiceReference(@NotNull StringLiteralExpression element) {
        super(element);

        serviceId = element.getContents();
    }

    @NotNull
    @Override
    public ResolveResult[] multiResolve(boolean incompleteCode) {
        return new ResolveResult[]{};
    }

    @NotNull
    @Override
    public Object[] getVariants() {
        PhpIndex phpIndex = PhpIndex.getInstance(getElement().getProject());
        List<LookupElement> results = new ArrayList<LookupElement>();
        Collection<PhpClass> phpClasses = phpIndex.getClassesByFQN("Zend\\ServiceManager\\ServiceManager");
        if (phpClasses.size() > 0) {
            results.add(new ServiceLookupElement(serviceId, phpClasses.iterator().next()));
        }
        return results.toArray();
    }
}
