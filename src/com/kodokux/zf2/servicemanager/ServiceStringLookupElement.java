package com.kodokux.zf2.servicemanager;

import com.intellij.codeInsight.lookup.LookupElement;
import com.intellij.codeInsight.lookup.LookupElementPresentation;
import com.kodokux.zf2.Zf2Icons;
import org.jetbrains.annotations.NotNull;

/**
 * User: johna
 * Date: 2013/08/17
 * Time: 18:39
 */
public class ServiceStringLookupElement extends LookupElement {

    private String serviceId;
    private String serviceClass;

    public ServiceStringLookupElement(String serviceId, String serviceClass) {
        this.serviceId = serviceId;
        this.serviceClass = serviceClass;
    }


    @NotNull
    @Override
    public String getLookupString() {
        return serviceId;
    }

    public void renderElement(LookupElementPresentation presentation) {
        presentation.setItemText(getLookupString());
        presentation.setTypeText(serviceClass);
        presentation.setTypeGrayed(true);
        presentation.setIcon(Zf2Icons.ZF2_ICON_16x16);
    }

}
