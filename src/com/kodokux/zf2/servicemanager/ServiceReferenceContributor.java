package com.kodokux.zf2.servicemanager;

import com.intellij.codeInsight.lookup.LookupElement;
import com.intellij.patterns.PlatformPatterns;
import com.intellij.psi.*;
import com.intellij.util.ProcessingContext;
import com.jetbrains.php.lang.PhpLanguage;
import com.jetbrains.php.lang.psi.elements.MethodReference;
import com.jetbrains.php.lang.psi.elements.ParameterList;
import com.jetbrains.php.lang.psi.elements.StringLiteralExpression;
import com.kodokux.zf2.Zf2Component;
import com.kodokux.zf2.util.Zf2InterfacesUtil;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: johna
 * Date: 2013/08/17
 * Time: 18:49
 * To change this template use File | Settings | File Templates.
 */
public class ServiceReferenceContributor extends PsiReferenceContributor {
    @Override
    public void registerReferenceProviders(PsiReferenceRegistrar psiReferenceRegistrar) {
        psiReferenceRegistrar.registerReferenceProvider(
                PlatformPatterns.psiElement(StringLiteralExpression.class).withLanguage(PhpLanguage.INSTANCE),
                new PsiReferenceProvider() {
                    @NotNull
                    @Override
                    public PsiReference[] getReferencesByElement(@NotNull PsiElement psiElement, @NotNull ProcessingContext processingContext) {

                        if (!Zf2Component.isEnabled(psiElement) || !(psiElement.getContext() instanceof ParameterList)) {
                            return new PsiReference[0];
                        }

                        ParameterList parameterList = (ParameterList) psiElement.getContext();

                        if (parameterList == null || !(parameterList.getContext() instanceof MethodReference)) {
                            return new PsiReference[0];
                        }
                        MethodReference method = (MethodReference) parameterList.getContext();

                        Zf2InterfacesUtil interfacesUtil = new Zf2InterfacesUtil();
                        if (!interfacesUtil.isServiceManagerGetCall(method)) {
                            return new PsiReference[0];
                        }

                        return new PsiReference[]{ new ServiceReference((StringLiteralExpression) psiElement) };
                    }
                }
        );
    }
}
